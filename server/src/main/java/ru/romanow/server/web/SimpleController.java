package ru.romanow.server.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by romanow on 17.11.16
 */
@RestController
public class SimpleController {

    @GetMapping("/ping")
    public String ping() {
        return "OK";
    }

    @GetMapping("/long")
    public String longRequest()
            throws InterruptedException {
        Thread.sleep(5000);
        return "OK";
    }
}
