package ru.romanow.hystrix.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by ronin on 19.11.16
 */
@FeignClient(name = "sample-server")
public interface RestClient {

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public String wrongRequest();

    @RequestMapping(value = "/long", method = RequestMethod.GET)
    public String longRequest();

    @RequestMapping(value = "/ping", method = RequestMethod.GET)
    public String correctRequest();
}
