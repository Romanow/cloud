package ru.romanow.hystrix.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.netflix.hystrix.contrib.javanica.conf.HystrixPropertiesManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by romanow on 16.11.16
 */
@Service
public class RestService {
    private static final Logger logger = LoggerFactory.getLogger(RestService.class);

    @Autowired
    private RestClient restClient;

    @HystrixCommand(fallbackMethod = "fallback")
    public String makeFallbackRequest() {
        return restClient.wrongRequest();
    }

    @HystrixCommand(
            fallbackMethod = "fallback"
//            commandProperties = @HystrixProperty(name = HystrixPropertiesManager.EXECUTION_ISOLATION_THREAD_TIMEOUT_IN_MILLISECONDS, value = "1000")
    )
    public String makeLongRequest() {
        return restClient.longRequest();
    }

    @HystrixCommand(fallbackMethod = "fallback")
    public String makeCorrectRequest() {
        return restClient.correctRequest();
    }

    public String fallback() {
        logger.info("Fallback method");
        return "fallback";
    }
}
