package ru.romanow.hystrix.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.romanow.hystrix.service.RestService;

/**
 * Created by romanow on 17.11.16
 */
@RestController
public class TestController {

    @Autowired
    private RestService restService;

    @GetMapping("/correct")
    public String correct() {
        return restService.makeCorrectRequest();
    }

    @GetMapping("/wrong")
    public String wrong() {
        return restService.makeFallbackRequest();
    }

    @GetMapping("/long")
    public String longRequest() {
        return restService.makeLongRequest();
    }
}
